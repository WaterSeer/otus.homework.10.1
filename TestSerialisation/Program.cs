﻿using Newtonsoft.Json;
using System;
using System.Text;

namespace TestSerialisation
{
    class Program
    {
        static Message m;
        static void Main(string[] args)
        {
            m = new Message();
            m.UserName = "WS";
            m.Content = "Вошёл в систему";
            m.ReceiveTime = DateTime.UtcNow;

            string message = JsonConvert.SerializeObject(m);
            byte[] data = Encoding.Unicode.GetBytes(message);
            Console.WriteLine(message);
            Console.WriteLine(  );
            Console.WriteLine();

            Message receiveMessage = JsonConvert.DeserializeObject<Message>(message);
            Console.WriteLine(receiveMessage.ToString());
            Console.WriteLine(receiveMessage.Content.ToString());

            Console.ReadKey();

        }
    }
}
