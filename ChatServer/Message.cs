﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChatServer
{
    public class Message
    {
        public string UserName { get; set; }
        public string Content { get; set; }
        public DateTime ReceiveTime { get; set; }
        public override string ToString()
        {
            return UserName + " [" + ReceiveTime.ToString("hh:mm:ss") + "]: " + Content;
            //ReceiveTime.ToShortTimeString()+":  "+Content; 
        }
    }
}

