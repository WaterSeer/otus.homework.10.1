﻿using Newtonsoft.Json;
using System;
using System.Net.Sockets;
using System.Text;

namespace ChatServer
{
    class ClientObject
    {
        protected internal string Id { get; private set; }
        protected internal NetworkStream Stream { get; private set; }        
        TcpClient client;
        ServerObject server; // объект сервера

        public ClientObject(TcpClient tcpClient, ServerObject serverObject)
        {
            Id = Guid.NewGuid().ToString();
            client = tcpClient;
            server = serverObject;
            serverObject.AddConnection(this);
        }

        public void Process()
        {
            try
            {
                Stream = client.GetStream();
                // получаем имя пользователя                
                // посылаем сообщение о входе в чат всем подключенным пользователям 
                Message message;
                message = GetMessage();
                Console.WriteLine(message.ToString());
                server.BroadcastMessage(JsonConvert.SerializeObject(message), this.Id);
                // в бесконечном цикле получаем сообщения от клиента
                while (true)
                {
                    try
                    {
                        message = GetMessage();
                        Console.WriteLine(message.ToString());
                        server.BroadcastMessage(JsonConvert.SerializeObject(message), this.Id);
                    }
                    catch
                    {
                        message.Content = "Покинул чат";
                        Console.WriteLine(message.ToString());
                        server.BroadcastMessage(JsonConvert.SerializeObject(message), this.Id);
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                // в случае выхода из цикла закрываем ресурсы
                server.RemoveConnection(this.Id);
                Close();
            }
        }

        // чтение входящего сообщения и преобразование в строку
        //private string GetMessage()
        private Message GetMessage()
        {
            byte[] data = new byte[64]; // буфер для получаемых данных
            StringBuilder builder = new StringBuilder();
            int bytes = 0;
            do
            {
                bytes = Stream.Read(data, 0, data.Length);
                builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
            }
            while (Stream.DataAvailable);

            Message receiveMessage;
            try
            {
                receiveMessage = JsonConvert.DeserializeObject<Message>(builder.ToString());
            }
            catch (JsonSerializationException ex)
            {
                receiveMessage = new Message();
                receiveMessage.UserName = "Не удалось десериализовать данные от клиента";
                receiveMessage.Content = ex.Message;
            }            
            return receiveMessage;
        }

        // закрытие подключения
        protected internal void Close()
        {
            if (Stream != null)
                Stream.Close();
            if (client != null)
                client.Close();
        }
    }
}

