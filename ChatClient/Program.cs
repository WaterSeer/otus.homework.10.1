﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Xml;

namespace ChatClient
{
    public class Program
    {

        public string Host { get; set; }
        public int Port { get; set; }        
        static Message messageBag;
        static TcpClient client;
        static NetworkStream stream;

        static void Main(string[] args)
        {      
            
            
            messageBag = new Message();
            Console.Write("Введите свое имя: ");
            messageBag.UserName = Console.ReadLine();
            client = new TcpClient();
            try
            {
                XmlDocument xml = new XmlDocument();
                xml.Load("web.config");
                XmlElement node = xml.SelectSingleNode("/configuration/ConnectionConfig") as XmlElement;

                client.Connect(node.SelectSingleNode("host").InnerText, Int32.Parse(node.SelectSingleNode("port").InnerText)); //подключение клиента
                stream = client.GetStream(); // получаем поток
                messageBag.ReceiveTime = DateTime.UtcNow;
                messageBag.Content = " Вошёл в чат ";

                string message = JsonConvert.SerializeObject(messageBag);
                byte[] data = Encoding.Unicode.GetBytes(message);
                stream.Write(data, 0, data.Length);

                // запускаем новый поток для получения данных
                Thread receiveThread = new Thread(new ThreadStart(ReceiveMessage));
                receiveThread.Start(); //старт потока
                Console.WriteLine("Добро пожаловать, {0}", messageBag.UserName);
                SendMessage();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Disconnect();
            }
        }
        // отправка сообщений
        static void SendMessage()
        {
            Console.WriteLine("Введите сообщение: ");

            while (true)
            {
                messageBag.Content = Console.ReadLine();
                messageBag.ReceiveTime = DateTime.UtcNow;
                byte[] data = Encoding.Unicode.GetBytes(JsonConvert.SerializeObject(messageBag));
                stream.Write(data, 0, data.Length);
            }
        }
        // получение сообщений
        static void ReceiveMessage()
        {
            while (true)
            {
                try
                {
                    byte[] data = new byte[64]; // буфер для получаемых данных
                    StringBuilder builder = new StringBuilder();
                    int bytes = 0;
                    do
                    {
                        bytes = stream.Read(data, 0, data.Length);
                        builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                    }
                    while (stream.DataAvailable);
                    Message receiveMessage = JsonConvert.DeserializeObject<Message>(builder.ToString());
                    Console.WriteLine(receiveMessage.ToString());
                }
                catch (JsonSerializationException ex)
                {                    
                    messageBag.UserName = "Не удалось десериализовать данные от сервера";
                    messageBag.Content = ex.Message;
                }
                catch
                {
                    Console.WriteLine("Подключение прервано!"); //соединение было прервано
                    Console.ReadLine();
                    Disconnect();
                }
            }
        }

        static void GetConnectoinSettings()
        {
            
        }

        static void Disconnect()
        {
            if (stream != null)
                stream.Close();//отключение потока
            if (client != null)
                client.Close();//отключение клиента
            Environment.Exit(0); //завершение процесса
        }
    }
}
